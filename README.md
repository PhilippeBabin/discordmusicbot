# DiscordMusicBot

This is a simple open source, self-hosted music bot. It's really simple: just write: \play `url` and you're good to go! <br>
<br>
This bot needs the following permissions: <br>
 - Send Messages<br>
 - Embed Links<br>
 - Read Message History<br>
 - Connect<br>
 - Speak<br>
 - Use Voice Activity<br>