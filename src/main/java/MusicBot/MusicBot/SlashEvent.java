package MusicBot.MusicBot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import org.apache.commons.lang3.SystemUtils;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.managers.AudioManager;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;

public class SlashEvent extends ListenerAdapter {
	AudioService audioService;
	private static AudioManager audioManager;

	private static final String folderSeperator = SystemUtils.IS_OS_LINUX ? "/" : "\\";
	private static final String songDir = System.getProperty("user.dir") + folderSeperator + "tmp" + folderSeperator;

	public SlashEvent() {
		audioService = new AudioService();
	}

	public void createSlashCommands(JDA builder) {
		CommandListUpdateAction commands = builder.updateCommands();

		commands.addCommands(
				Commands.slash("play", "Play the music you want.")
						.addOptions(new OptionData(OptionType.STRING, "url", "Youtube URL")
								.setRequired(false))
						.addOptions(new OptionData(OptionType.ATTACHMENT, "file", "mp3 file")
								.setRequired(false))
						.addOptions(new OptionData(OptionType.BOOLEAN, "shuffle", "Shuffles playlist.")
								.setRequired(false)));
		commands.addCommands(Commands.slash("skip", "Skip current song."));
		commands.addCommands(Commands.slash("stop", "Stop current song and clears queue."));
		commands.addCommands(Commands.slash("pause", "Pauses current song."));
		commands.addCommands(Commands.slash("resume", "Resumes a paused song."));
		commands.addCommands(Commands.slash("volume", "Set the volume between 0% and 150%.")
				.addOptions(new OptionData(OptionType.INTEGER, "volume", "volume value")
						.setRequired(true)));
		commands.addCommands(Commands.slash("repeat", "Repeats the current song."));
		commands.addCommands(Commands.slash("queue", "See all songs in queue."));
		commands.queue();
	}

	@Override
	public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
		if (event.getGuild() == null)
			return;

		audioManager = event.getGuild().getAudioManager();
		GuildMusicManager musicManager = audioService.getGuildAudioPlayer(event.getGuild());
		switch (event.getName()) {
			case "play":
				play(event);
				break;
			case "skip":
				skipTrack(event);
				break;
			case "stop":
				stopAll(musicManager, event);
				break;
			case "pause":
				pauseCommand(musicManager, event);
				break;
			case "resume":
				resumeCommand(musicManager, event);
				break;
			case "repeat":
				repeat(musicManager, event);
				break;
			case "queue":
				showQueue(musicManager.getScheduler(), event);
				break;
			case "volume":
				setVolume(musicManager, event);
				break;
		}
	}

	private void play(SlashCommandInteractionEvent event) {
		List<OptionMapping> options = event.getOptions();
		String url = null;
		Attachment attachment = null;
		Boolean shuffle = null;
		// Get all options
		for (int i = 0; i < options.size(); i++) {
			OptionMapping command = event.getOptions().get(i);
			switch (command.getName()) {
				case "url":
					url = event.getOption("url").getAsString();
					// Do not play if we have a file
					if (attachment != null){
						event.reply("Please provide one URL or one file at a time.").queue();
						return;
					}
					break;
				case "file":
					attachment = event.getOption("file").getAsAttachment();
					// Do not play if we have a URL
					if (url != null){
						event.reply("Please provide one URL or one file at a time.").queue();
						return;
					}
					break;
				case "shuffle":
					shuffle = event.getOption("shuffle").getAsBoolean();
					/* Failsafe just in case, note: Discord has client-side checks, and doesn't allow empty values */
					if (shuffle == null) {
						event.reply("Invalid shuffle value: Must be True or False").queue();
						return;
					}
					break;
			}
		}

		// Play the attached file
		if (url != null) {
			if (shuffle != null) {
				audioService.loadAndPlay(event, url, shuffle, event.getMember());
			} else {
				audioService.loadAndPlay(event, url, false, event.getMember());
			}
		} else if (attachment != null) {	// Play the attached file (download it first, check validity)
			CompletableFuture<File> downloadedFile = attachment.getProxy().downloadToFile(new File(songDir + System.currentTimeMillis()
			+ "_" + attachment.getFileName()));
			while (!downloadedFile.isDone()) {
			}

			try {
				File file = downloadedFile.get();
				String downloadedFilePath = file.getAbsolutePath();
				if (isMP3File(downloadedFilePath)) {
					if (shuffle != null) {
						audioService.loadAndPlay(event, downloadedFilePath, shuffle, event.getMember());
					} else {
						audioService.loadAndPlay(event, downloadedFilePath, false, event.getMember());
					}
					return;
				}
				if(!file.delete()) {
					System.err.println("Failed to delete file " + file.getName());
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean isMP3File(String path) {
		Pattern p = Pattern.compile(".(mp3)$");
		Matcher m = p.matcher(path);
		return m.find();
	}

	private void showQueue(TrackScheduler scheduler, SlashCommandInteractionEvent event) {
		List<String> nameOfSongsArray = new ArrayList<String>();
		String tempString = "";
		int index = 1;
		if (scheduler.getQueue().isEmpty()) {
			event.reply("Queue is empty.").queue();
			return;
		}
		for (AudioTrack track : scheduler.getQueue()) {
			tempString += index + " - " + track.getInfo().title + "\n";
			index++;
			if (tempString.length() >= 1900) {
				nameOfSongsArray.add(tempString);
				tempString = "";
			}
		}
		;
		if (!nameOfSongsArray.contains(tempString)) {
			nameOfSongsArray.add(tempString);
		}
		event.reply("🕔 Songs in queue 🕔 \n" + nameOfSongsArray.get(0)).queue();
		for (int i = 1; i < nameOfSongsArray.size(); i++) {
			event.getChannel().asTextChannel().sendMessage(nameOfSongsArray.get(i)).queue();
		}

	}

	private void repeat(GuildMusicManager musicManager, SlashCommandInteractionEvent event) {
		boolean isRepeating = !musicManager.getScheduler().getRepeating();
		musicManager.getScheduler().setRepeating(isRepeating);
		event.reply("🔄 Repeat 🔄 \n" + "Repeat is " + (isRepeating ? "on" : "off"))
				.addActionRow(Button.primary("repeat", "Turn off repeat"))
				.queue();
	}

	private void pauseCommand(GuildMusicManager musicManager, SlashCommandInteractionEvent event) {
		if (musicManager.getPlayer().getPlayingTrack() != null) {
			event.reply("⏸️ Paused song ⏸️ \n" + musicManager.getPlayer().getPlayingTrack().getInfo().title)
					.addActionRow(Button.primary("resume", "Resume song"))
					.queue();
			musicManager.getPlayer().setPaused(true);
		}
	}

	private void resumeCommand(GuildMusicManager musicManager, SlashCommandInteractionEvent event) {
		if (musicManager.getPlayer().getPlayingTrack() != null) {
			event.reply("⏯️ Resumed song ⏯️ \n" + musicManager.getPlayer().getPlayingTrack().getInfo().title)
					.addActionRow(Button.primary("pause", "Pause song"))
					.queue();
			musicManager.getPlayer().setPaused(false);
		}
	}

	private void setVolume(GuildMusicManager musicManager, SlashCommandInteractionEvent event) {
		try {
			int volume = (int) event.getOption("volume").getAsLong();
			if (volume >= 0 && volume <= 150) {
				musicManager.getPlayer().setVolume(volume);
				event.reply("🎚️ Volume changed 🎚️ \n" + "Volume: " + volume + "%").queue();
				return;
			}
		} catch (Exception e) {
		}
		event.reply("❌ Error setting volume ❌ \n" + "Volume isn't a number or isn't between 0 and 150.").queue();
	}

	private void skipTrack(SlashCommandInteractionEvent event) {
		GuildMusicManager musicManager = audioService.getGuildAudioPlayer(event.getGuild());
		boolean isNextSong = musicManager.getScheduler().nextTrack();
		AudioTrack track = musicManager.getPlayer().getPlayingTrack();
		String replyMessage = "";
		replyMessage += "☑️ Skipped song ☑️";
		if (isNextSong) {
			replyMessage += "\nNow playing: " + track.getInfo().title;
		}
		event.reply(replyMessage).queue();
	}

	private void stopAll(GuildMusicManager musicManager, @Nullable SlashCommandInteractionEvent event) {
		musicManager.getScheduler().getQueue().clear();
		musicManager.getScheduler().nextTrack();
		if (event != null)
			event.reply("Stopped all songs.").queue();
	}

	public static void disconnectFromVoiceChannel() {
		if (audioManager != null) {
			audioManager.closeAudioConnection();
		}
	}

	@Override
	public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
		if (event.getChannelJoined() != null) {
			return;
		}
		User bot = event.getJDA().getUserById(event.getJDA().getSelfUser().getIdLong());
		User disconnectedUser = event.getMember().getUser();
		if (disconnectedUser.equals(bot)) {
			stopAll(audioService.getGuildAudioPlayer(event.getGuild()), null);
		}
	}

	@Override
	public void onButtonInteraction(ButtonInteractionEvent event) {
		String type = event.getComponentId();

		event.deferEdit().queue();
		GuildMusicManager musicManager = audioService.getGuildAudioPlayer(event.getGuild());

		switch (type) {
			case "repeat":
				boolean isRepeating = !musicManager.getScheduler().getRepeating();
				musicManager.getScheduler().setRepeating(isRepeating);
				break;
			case "resume":
				if (musicManager.getPlayer().getPlayingTrack() != null) {
					musicManager.getPlayer().setPaused(false);
				}
				break;
			case "pause":
				if (musicManager.getPlayer().getPlayingTrack() != null) {
					musicManager.getPlayer().setPaused(true);
				}
				break;
		}
	}
}
