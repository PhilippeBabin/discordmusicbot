package MusicBot.MusicBot;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import dev.lavalink.youtube.YoutubeAudioSourceManager;
import dev.lavalink.youtube.clients.Android;
import dev.lavalink.youtube.clients.AndroidMusic;
import dev.lavalink.youtube.clients.AndroidTestsuite;
import dev.lavalink.youtube.clients.MediaConnect;
import dev.lavalink.youtube.clients.Music;
import dev.lavalink.youtube.clients.Web;
import dev.lavalink.youtube.clients.WebEmbedded;
import dev.lavalink.youtube.clients.skeleton.Client;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.managers.AudioManager;

public class AudioService {
	private final Map<Long, GuildMusicManager> musicManagers;
	private final AudioPlayerManager playerManager;

	public AudioService() {
		musicManagers = new HashMap<>();

		playerManager = new DefaultAudioPlayerManager();
		YoutubeAudioSourceManager ytSourceManager = new dev.lavalink.youtube.YoutubeAudioSourceManager(true, new Client[] {new WebEmbedded(), new Web(), new Music(), new AndroidTestsuite(), new MediaConnect()});
		ytSourceManager.useOauth2("", true);
		playerManager.registerSourceManager(ytSourceManager);
		AudioSourceManagers.registerLocalSource(playerManager);	// Required to play local files
		AudioSourceManagers.registerRemoteSources(playerManager);
		playerManager.getConfiguration().setFilterHotSwapEnabled(true);
	}

	public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
		long guildId = Long.parseLong(guild.getId());
		GuildMusicManager musicManager = musicManagers.get(guildId);

		if (musicManager == null) {
			musicManager = new GuildMusicManager(playerManager);
			musicManagers.put(guildId, musicManager);
		}

		guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

		return musicManager;
	}

	public void loadAndPlay(final SlashCommandInteractionEvent event, final String trackUrl, boolean shuffle,
			Member member) {
		TextChannel channel = event.getChannel().asTextChannel();

		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

		playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
			@Override
			public void trackLoaded(AudioTrack track) {
				play(channel.getGuild(), musicManager, track, member, event);
				event.reply("🎵 Adding to queue 🎵 \n"
						+ (track.getInfo().title.equals("Unknown title") ? "MP3 File" : track.getInfo().title)).queue();
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				if (shuffle) {
					Collections.shuffle(playlist.getTracks());
				}
				for (AudioTrack track : playlist.getTracks()) {
					if (!play(channel.getGuild(), musicManager, track, member, event)) {
						break;
					}
				}
				event.reply("🎵 Adding all songs to queue 🎵 \n" + playlist.getName()).queue();
			}

			@Override
			public void noMatches() {
				event.reply("❌ Error playing song ❌ \n" + "URL doesn't exist or doesn't work.").queue();
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				System.out.println(exception);
				event.reply("❌ Error playing song ❌ \n" + "Could not play song.").queue();
			}
		});
	}

	private boolean play(Guild guild, GuildMusicManager musicManager, AudioTrack track, Member member,
			SlashCommandInteractionEvent event) {
		if (connectToVoiceChannel(guild.getAudioManager(), member, event)) {
			musicManager.getScheduler().queue(track);
			return true;
		}
		return false;
	}

	private static boolean connectToVoiceChannel(AudioManager audioManager, Member member,
			SlashCommandInteractionEvent event) {
		VoiceChannel connectedChannel = member.getVoiceState().getChannel().asVoiceChannel();
		if (connectedChannel == null) {
			event.reply("❌ Error playing song ❌ \n " + "You are not connected to voice channel.").queue();
			return false;
		} else {
			if (!audioManager.isConnected()) {
				try {
					audioManager.openAudioConnection(connectedChannel);
					audioManager.setSelfDeafened(true);
					return true;
				} catch (Exception e) {
					if (e instanceof InsufficientPermissionException) {
						event.reply("❌ Error connecting to vocal channel ❌ \n"
								+ "I do not have the permissions to join the channel.").queue();
						return false;
					}
				}
			}
		}
		return true;
	}
}
