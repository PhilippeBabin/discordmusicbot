package MusicBot.MusicBot;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class schedules tracks for the audio player. It contains the queue of
 * tracks.
 */
public class TrackScheduler extends AudioEventAdapter {
	private final AudioPlayer player;
	private final BlockingQueue<AudioTrack> queue;
	private boolean repeating;
	private boolean isNightcore;

	public BlockingQueue<AudioTrack> getQueue() {
		return queue;
	}
	
	public boolean isNightcore() {
		return isNightcore;
	}

	public void setNightcore(boolean isNightcore) {
		this.isNightcore = isNightcore;
	}
	
	public void setRepeating(boolean repeating) {
		this.repeating = repeating;
	}
	
	public boolean getRepeating() {
		return repeating;
	}

	/**
	 * @param player The audio player this scheduler uses
	 */
	public TrackScheduler(AudioPlayer player) {
		this.player = player;
		this.queue = new LinkedBlockingQueue<>();
		this.repeating = false;
	}

	/**
	 * Add the next track to queue or play right away if nothing is in the queue.
	 *
	 * @param track The track to play or add to queue.
	 */
	public void queue(AudioTrack track) {
		// Calling startTrack with the noInterrupt set to true will start the track only
		// if nothing is currently playing. If
		// something is playing, it returns false and does nothing. In that case the
		// player was already playing so this
		// track goes to the queue instead.
		if (!player.startTrack(track, true)) {
			queue.offer(track);
		}
	}

	/**
	 * Start the next track, stopping the current one if it is playing.
	 */
	public boolean nextTrack() {
		// Start the next track, regardless of if something is already playing or not.
		// In case queue was empty, we are
		// giving null to startTrack, which is a valid argument and will simply stop the
		// player.
		isNightcore = false;
		player.setFilterFactory(null);
		return player.startTrack(queue.poll(), false);
	}

	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
		AudioTrack lastTrack = track;
		player.setFilterFactory(null);
		isNightcore = false;
		if (endReason.mayStartNext) {
			if (repeating) {
				
				player.startTrack(lastTrack.makeClone(), false);
				return;
			}
            if (!nextTrack()) {
            	SlashEvent.disconnectFromVoiceChannel();
                if (isMP3File(track.getInfo().uri)) {
                	File mp3File = new File(track.getInfo().uri);
                	mp3File.delete();
                }
            }
            return;
        }
        if (queue.size() == 0 && player.getPlayingTrack() == null) {
        	SlashEvent.disconnectFromVoiceChannel();
        	if (isMP3File(track.getInfo().uri)) {
	        	File mp3File = new File(track.getInfo().uri);
	        	mp3File.delete();
        	}
        }
	}
	
	private static boolean isMP3File(String path) {
        Pattern p = Pattern.compile(".(mp3)$");
        Matcher m = p.matcher(path);
        return m.find();
    }
}
